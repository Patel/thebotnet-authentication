﻿Imports System.IO
Imports System.Net
Imports System.Text
Public Class frmAuth

    Dim BotName As String = "theBOTNET Authentication (Patel Edition)"
    Dim MinimumPosts As Integer = 20
    Dim MinimumReputation As Integer = 1
    Dim MinimumSubscription As Boolean = False
    Dim theBOTNETReferralLink As String = "http://www.thebotnet.com/index.php?referrerid=25486"

    Dim ResponseAvailable As Boolean = False
    Dim ResponseValue As String = ""
    Private Sub frmAuth_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblBotName.Text = "Bot Name: " & BotName
        lblBotName.Left = Me.ClientSize.Width \ 2 - lblBotName.Width \ 2

        lblReqPosts.Text = "Posts: 0/" & MinimumPosts.ToString
        lblReqReputation.Text = "Reputation: 0/" & MinimumReputation.ToString
        lblReqSubscription.Text = "Subscription: No/" & boolToString(MinimumSubscription)
    End Sub

    Private Sub btnSignUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSignUp.Click
        Process.Start(theBOTNETReferralLink)
    End Sub

    Private Sub btnFetch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFetch.Click
        btnFetch.Enabled = False
        btnAuthenticate.Enabled = False

        bgwRequest.RunWorkerAsync()
        Do Until ResponseAvailable = True
            Application.DoEvents()
        Loop
        ResponseAvailable = False

        Dim RawXMLAuth As New Xml.XmlDocument
        RawXMLAuth.LoadXml(ResponseValue)

        Dim Username As String = RawXMLAuth.Item("user").Item("username").InnerText
        Dim Posts As Integer = Val(RawXMLAuth.Item("user").Item("posts").InnerText)
        Dim Reputation As Integer = Val(RawXMLAuth.Item("user").Item("reputation").InnerText)
        Dim Subscription As Boolean = Convert.ToBoolean(RawXMLAuth.Item("user").Item("subscription").InnerText)

        lblUserInfo.Text = "Username: " & Username
        lblPostsInfo.Text = "Posts: " & Posts.ToString
        lblReputationInfo.Text = "Reputation: " & Reputation.ToString
        lblSubscriptionInfo.Text = "Subscription: " & boolToString(Subscription)
        lblReqPosts.Text = "Posts: " & Posts.ToString & "/" & MinimumPosts.ToString
        lblReqReputation.Text = "Reputation: " & Reputation.ToString & "/" & MinimumReputation.ToString
        lblReqSubscription.Text = "Subscription: " & boolToString(Subscription) & "/" & boolToString(MinimumSubscription)

        If Username = "N/A" Then
            btnAuthenticate.Enabled = False
            MsgBox("Error 1: You don't have auth on this IP! Make a post on theBOTNET.com to get auth!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error 1")
            Exit Sub
        ElseIf Username = "BANNED USER" Then
            btnAuthenticate.Enabled = False
            MsgBox("Error 2: You're banned on theBOTNET!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error 2")
            Exit Sub
        Else
            If Subscription = True Then
                btnFetch.Enabled = False
                btnAuthenticate.Enabled = True
                Exit Sub
            ElseIf Subscription = False Then
                If MinimumSubscription = True Then
                    btnFetch.Enabled = True
                    btnAuthenticate.Enabled = False
                    MsgBox("Error 5: You don't have a subsctiption on theBOTNET!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error 5")
                    Exit Sub
                Else
                    If Posts >= MinimumPosts Then
                        If Reputation >= MinimumReputation Then
                            btnFetch.Enabled = False
                            btnAuthenticate.Enabled = True
                        Else
                            btnFetch.Enabled = True
                            btnAuthenticate.Enabled = False
                            MsgBox("Error 4: You don't have enough reputation!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error 3")
                            Exit Sub
                        End If
                    Else
                        btnFetch.Enabled = True
                        btnAuthenticate.Enabled = False
                        MsgBox("Error 3: You don't have enough posts!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error 2")
                        Exit Sub
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub btnAuthenticate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAuthenticate.Click
        'Insert code to call new form and hide this form like this (be sure to remove the msgbox):
        'frmMain.Show
        'Me.Hide
        'Remember to add End() in the FormClosing on your main form

        MsgBox("Congratulations! You passed the auth test!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Congratulations!")
    End Sub

    Public Function boolToString(ByVal bool As Boolean)
        Dim boolString As String
        If bool Then boolString = "Yes" Else boolString = "No"
        Return boolString
    End Function

    Private Sub bgwRequest_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwRequest.DoWork
        Dim encoding As New UTF8Encoding
        Dim byteData As Byte() = encoding.GetBytes("")

        Dim postReq As HttpWebRequest = DirectCast(WebRequest.Create("http://thebotnet.com/api/post.php?xml"), HttpWebRequest)
        postReq.Proxy = New WebProxy
        postReq.Method = "POST"
        postReq.KeepAlive = True
        postReq.ContentType = "application/x-www-form-urlencoded"
        postReq.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.2.3) Gecko/20100401 Firefox/4.0 (.NET CLR 3.5.30729)"
        postReq.ContentLength = byteData.Length

        Dim postreqstream As Stream = postReq.GetRequestStream()
        postreqstream.Write(byteData, 0, byteData.Length)
        postreqstream.Close()
        Dim postresponse As HttpWebResponse

        postresponse = DirectCast(postReq.GetResponse(), HttpWebResponse)
        Dim postreqreader As New StreamReader(postresponse.GetResponseStream())

        ResponseValue = postreqreader.ReadToEnd
        ResponseAvailable = True
    End Sub
End Class