﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAuth
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAuth))
        Me.bgwRequest = New System.ComponentModel.BackgroundWorker()
        Me.btnSignUp = New System.Windows.Forms.Button()
        Me.btnAuthenticate = New System.Windows.Forms.Button()
        Me.btnFetch = New System.Windows.Forms.Button()
        Me.lblInfoLine2 = New System.Windows.Forms.Label()
        Me.lblInfoLine1 = New System.Windows.Forms.Label()
        Me.grpBotRequirements = New System.Windows.Forms.GroupBox()
        Me.lblReqSubscription = New System.Windows.Forms.Label()
        Me.lblReqReputation = New System.Windows.Forms.Label()
        Me.lblReqPosts = New System.Windows.Forms.Label()
        Me.lblPostsInfo = New System.Windows.Forms.Label()
        Me.lblSubscriptionInfo = New System.Windows.Forms.Label()
        Me.lblReputationInfo = New System.Windows.Forms.Label()
        Me.grpUserInformation = New System.Windows.Forms.GroupBox()
        Me.lblUserInfo = New System.Windows.Forms.Label()
        Me.lblBotName = New System.Windows.Forms.Label()
        Me.lblTBNAuthentication = New System.Windows.Forms.Label()
        Me.grpBotRequirements.SuspendLayout()
        Me.grpUserInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSignUp
        '
        Me.btnSignUp.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSignUp.Location = New System.Drawing.Point(9, 170)
        Me.btnSignUp.Name = "btnSignUp"
        Me.btnSignUp.Size = New System.Drawing.Size(336, 33)
        Me.btnSignUp.TabIndex = 17
        Me.btnSignUp.Text = "Don't have an account? Sign up here!"
        Me.btnSignUp.UseVisualStyleBackColor = True
        '
        'btnAuthenticate
        '
        Me.btnAuthenticate.Enabled = False
        Me.btnAuthenticate.Location = New System.Drawing.Point(180, 209)
        Me.btnAuthenticate.Name = "btnAuthenticate"
        Me.btnAuthenticate.Size = New System.Drawing.Size(165, 33)
        Me.btnAuthenticate.TabIndex = 16
        Me.btnAuthenticate.Text = "Authenticate Program"
        Me.btnAuthenticate.UseVisualStyleBackColor = True
        '
        'btnFetch
        '
        Me.btnFetch.Location = New System.Drawing.Point(9, 209)
        Me.btnFetch.Name = "btnFetch"
        Me.btnFetch.Size = New System.Drawing.Size(165, 33)
        Me.btnFetch.TabIndex = 15
        Me.btnFetch.Text = "Fetch User Information"
        Me.btnFetch.UseVisualStyleBackColor = True
        '
        'lblInfoLine2
        '
        Me.lblInfoLine2.AutoSize = True
        Me.lblInfoLine2.Location = New System.Drawing.Point(75, 152)
        Me.lblInfoLine2.Name = "lblInfoLine2"
        Me.lblInfoLine2.Size = New System.Drawing.Size(205, 13)
        Me.lblInfoLine2.TabIndex = 14
        Me.lblInfoLine2.Text = "post on theBOTNET.com and try again."
        '
        'lblInfoLine1
        '
        Me.lblInfoLine1.AutoSize = True
        Me.lblInfoLine1.Location = New System.Drawing.Point(22, 139)
        Me.lblInfoLine1.Name = "lblInfoLine1"
        Me.lblInfoLine1.Size = New System.Drawing.Size(310, 13)
        Me.lblInfoLine1.TabIndex = 13
        Me.lblInfoLine1.Text = "In order to retrieve your user information, you must make a"
        '
        'grpBotRequirements
        '
        Me.grpBotRequirements.Controls.Add(Me.lblReqSubscription)
        Me.grpBotRequirements.Controls.Add(Me.lblReqReputation)
        Me.grpBotRequirements.Controls.Add(Me.lblReqPosts)
        Me.grpBotRequirements.Location = New System.Drawing.Point(180, 61)
        Me.grpBotRequirements.Name = "grpBotRequirements"
        Me.grpBotRequirements.Size = New System.Drawing.Size(165, 75)
        Me.grpBotRequirements.TabIndex = 12
        Me.grpBotRequirements.TabStop = False
        Me.grpBotRequirements.Text = "Bot Requirements:"
        '
        'lblReqSubscription
        '
        Me.lblReqSubscription.AutoSize = True
        Me.lblReqSubscription.Location = New System.Drawing.Point(6, 42)
        Me.lblReqSubscription.Name = "lblReqSubscription"
        Me.lblReqSubscription.Size = New System.Drawing.Size(112, 13)
        Me.lblReqSubscription.TabIndex = 4
        Me.lblReqSubscription.Text = "Subscription: No/No"
        '
        'lblReqReputation
        '
        Me.lblReqReputation.AutoSize = True
        Me.lblReqReputation.Location = New System.Drawing.Point(6, 29)
        Me.lblReqReputation.Name = "lblReqReputation"
        Me.lblReqReputation.Size = New System.Drawing.Size(87, 13)
        Me.lblReqReputation.TabIndex = 4
        Me.lblReqReputation.Text = "Reputation: 0/0"
        '
        'lblReqPosts
        '
        Me.lblReqPosts.AutoSize = True
        Me.lblReqPosts.Location = New System.Drawing.Point(6, 16)
        Me.lblReqPosts.Name = "lblReqPosts"
        Me.lblReqPosts.Size = New System.Drawing.Size(56, 13)
        Me.lblReqPosts.TabIndex = 4
        Me.lblReqPosts.Text = "Posts: 0/0"
        '
        'lblPostsInfo
        '
        Me.lblPostsInfo.AutoSize = True
        Me.lblPostsInfo.Location = New System.Drawing.Point(6, 29)
        Me.lblPostsInfo.Name = "lblPostsInfo"
        Me.lblPostsInfo.Size = New System.Drawing.Size(46, 13)
        Me.lblPostsInfo.TabIndex = 5
        Me.lblPostsInfo.Text = "Posts: 0"
        '
        'lblSubscriptionInfo
        '
        Me.lblSubscriptionInfo.AutoSize = True
        Me.lblSubscriptionInfo.Location = New System.Drawing.Point(6, 55)
        Me.lblSubscriptionInfo.Name = "lblSubscriptionInfo"
        Me.lblSubscriptionInfo.Size = New System.Drawing.Size(93, 13)
        Me.lblSubscriptionInfo.TabIndex = 6
        Me.lblSubscriptionInfo.Text = "Subscription: No"
        '
        'lblReputationInfo
        '
        Me.lblReputationInfo.AutoSize = True
        Me.lblReputationInfo.Location = New System.Drawing.Point(6, 42)
        Me.lblReputationInfo.Name = "lblReputationInfo"
        Me.lblReputationInfo.Size = New System.Drawing.Size(77, 13)
        Me.lblReputationInfo.TabIndex = 4
        Me.lblReputationInfo.Text = "Reputation: 0"
        '
        'grpUserInformation
        '
        Me.grpUserInformation.Controls.Add(Me.lblSubscriptionInfo)
        Me.grpUserInformation.Controls.Add(Me.lblReputationInfo)
        Me.grpUserInformation.Controls.Add(Me.lblPostsInfo)
        Me.grpUserInformation.Controls.Add(Me.lblUserInfo)
        Me.grpUserInformation.Location = New System.Drawing.Point(9, 61)
        Me.grpUserInformation.Name = "grpUserInformation"
        Me.grpUserInformation.Size = New System.Drawing.Size(165, 75)
        Me.grpUserInformation.TabIndex = 11
        Me.grpUserInformation.TabStop = False
        Me.grpUserInformation.Text = "User Information:"
        '
        'lblUserInfo
        '
        Me.lblUserInfo.AutoSize = True
        Me.lblUserInfo.Location = New System.Drawing.Point(6, 16)
        Me.lblUserInfo.Name = "lblUserInfo"
        Me.lblUserInfo.Size = New System.Drawing.Size(83, 13)
        Me.lblUserInfo.TabIndex = 4
        Me.lblUserInfo.Text = "Username: N/A"
        '
        'lblBotName
        '
        Me.lblBotName.AutoSize = True
        Me.lblBotName.Location = New System.Drawing.Point(40, 38)
        Me.lblBotName.Name = "lblBotName"
        Me.lblBotName.Size = New System.Drawing.Size(274, 13)
        Me.lblBotName.TabIndex = 10
        Me.lblBotName.Text = "Bot Name: theBOTNET Authentication (Patel Edition)"
        '
        'lblTBNAuthentication
        '
        Me.lblTBNAuthentication.AutoSize = True
        Me.lblTBNAuthentication.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTBNAuthentication.Location = New System.Drawing.Point(13, 7)
        Me.lblTBNAuthentication.Name = "lblTBNAuthentication"
        Me.lblTBNAuthentication.Size = New System.Drawing.Size(328, 30)
        Me.lblTBNAuthentication.TabIndex = 9
        Me.lblTBNAuthentication.Text = "theBOTNET.com Authentication"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(354, 251)
        Me.Controls.Add(Me.btnSignUp)
        Me.Controls.Add(Me.btnAuthenticate)
        Me.Controls.Add(Me.btnFetch)
        Me.Controls.Add(Me.lblInfoLine2)
        Me.Controls.Add(Me.lblInfoLine1)
        Me.Controls.Add(Me.grpBotRequirements)
        Me.Controls.Add(Me.grpUserInformation)
        Me.Controls.Add(Me.lblBotName)
        Me.Controls.Add(Me.lblTBNAuthentication)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "theBOTNET Authentication"
        Me.grpBotRequirements.ResumeLayout(False)
        Me.grpBotRequirements.PerformLayout()
        Me.grpUserInformation.ResumeLayout(False)
        Me.grpUserInformation.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bgwRequest As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnSignUp As System.Windows.Forms.Button
    Friend WithEvents btnAuthenticate As System.Windows.Forms.Button
    Friend WithEvents btnFetch As System.Windows.Forms.Button
    Friend WithEvents lblInfoLine2 As System.Windows.Forms.Label
    Friend WithEvents lblInfoLine1 As System.Windows.Forms.Label
    Friend WithEvents grpBotRequirements As System.Windows.Forms.GroupBox
    Friend WithEvents lblReqSubscription As System.Windows.Forms.Label
    Friend WithEvents lblReqReputation As System.Windows.Forms.Label
    Friend WithEvents lblReqPosts As System.Windows.Forms.Label
    Friend WithEvents lblPostsInfo As System.Windows.Forms.Label
    Friend WithEvents lblSubscriptionInfo As System.Windows.Forms.Label
    Friend WithEvents lblReputationInfo As System.Windows.Forms.Label
    Friend WithEvents grpUserInformation As System.Windows.Forms.GroupBox
    Friend WithEvents lblUserInfo As System.Windows.Forms.Label
    Friend WithEvents lblBotName As System.Windows.Forms.Label
    Friend WithEvents lblTBNAuthentication As System.Windows.Forms.Label

End Class
